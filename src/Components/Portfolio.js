import React, { Component } from 'react';

class Portfolio extends Component {
    render() {
        if(this.props.data){
          var portfolio = this.props.data.projects.map(function(project){
            var imageUrl = 'images/portfolio/'+project.image;
            var idModal = '#'+project.modal;
            return <div className="columns portfolio-item">
               <div className="item-wrap">
                  <a href={idModal} title="">
                     <img alt="" src={imageUrl} />
                     <div className="overlay">
                        <div className="portfolio-item-meta">
                            <h5>{project.title}</h5>
                            <p>{project.category}</p>
                        </div>
                     </div>
                     <div className="link-icon"><i className="icon-plus"></i></div>
                  </a>
               </div>
           </div>
          });

          var modals = this.props.data.projects.map(function(project){
            var imageMod = 'images/portfolio/modals/'+project.image;
            return <div id={project.modal} className="popup-modal mfp-hide">
                <img className="scale-with-grid" src={imageMod} alt="" />
                <div className="description-box">
                    <h4>{project.title}</h4>
                    <p>{project.description}</p>
                    <span className="categories"><i className="fa fa-tag"></i>{project.tags}</span>
                </div>
                <div className="link-box">
                    <a href="http://www.behance.net">Details</a>
                    <a className="popup-modal-dismiss">Close</a>
                </div>
            </div>
          });
        }

        return (
          <section id="portfolio">
              <div className="row">
                 <div className="twelve columns collapsed">
                    <h1>Check Out Some of My Works.</h1>
                    <div id="portfolio-wrapper" className="bgrid-quarters s-bgrid-thirds cf">
                       {portfolio}
                    </div>
                 </div>
              </div>

              {modals}
       </section>
        );
    }
}

export default Portfolio;
