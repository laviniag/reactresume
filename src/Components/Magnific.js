import React, { Component } from 'react';
import $ from 'jquery';

var Dialog = React.createClass({
    componentWillReceiveProps(newProps) {
        if (this.props.open !== newProps.open) {
            this.toggleDialog(newProps);
        }
    },
    render() {
        return ( < div ref = "mfpRoot" className = "white-popup mfp-hide" > {this.props.children} </div> );
    },
    toggleDialog(props) {
         if (props.open) {
                $.magnificPopup.open({
                    items: {
                        src: $(this.refs.mfpRoot),
                        type: 'inline'
                    },
                    callbacks: {
                    close: function() {
                        props.onClose();
                    }.bind(this)
                }
                });
         } else {
                $.magnificPopup.close();
         }
    }
});

var Magnific = React.createClass({
       getInitialState() {
            return {
                showDialog: false,
                now: Date.now()
            };
        },
        handleShowDialog() {
            this.setState({
                showDialog: true,
                now: Date.now()
            });
        },
        handleHideDialog() {
            this.setState({
                showDialog: false
            });
        },
        render() {
            return ( < div >
            <button onClick = {this.handleShowDialog}> Show Dialog </button> <Dialog open = {
                    this.state.showDialog
                }
            onClose = {
                this.handleHideDialog
            } >
            < div > {
                    this.state.now
                } </div> </Dialog > </div>
            );
        }
});

export default Magnific;